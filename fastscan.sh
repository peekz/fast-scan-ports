#!/bin/bash
function alarm() {
    timeout=$1; shift;
    bash -c "$@" &
    pid=$!
    {
      sleep $timeout
      kill $pid 2> /dev/null
    } &
    wait $pid 2> /dev/null
    return $?
  }
# fast scan
echo "[+] usage : <$0> <host>"
echo "[+] host : $1"
echo "[+] fast scan..."
for port in 21 22 25 53 80 139 443 445 1337 8000 8080 4443;do
	alarm 1 "echo >/dev/tcp/$1/$port" && echo "open port $port" || echo "closed port $port"
done
echo "[+] done"
